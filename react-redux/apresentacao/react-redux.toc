\select@language {english}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Conceitos fundamentais}{3}{0}{1}
\beamer@sectionintoc {2}{React}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Criando um projeto}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Adicionando componentes}{5}{0}{2}
\beamer@sectionintoc {3}{Redux}{6}{0}{3}
\beamer@subsectionintoc {3}{1}{Fundamentos}{6}{0}{3}
\beamer@sectionintoc {4}{Exemplos}{7}{0}{4}
\beamer@sectionintoc {5}{Estudo Complementar}{8}{0}{5}
\beamer@sectionintoc {6}{Pr\IeC {\'a}tica}{9}{0}{6}
