import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';



// ---------------------------------------------------

// Adição do suporte a Redux:

import { createStore, applyMiddleware } from 'redux'
import amigoReducer from './reducers/Amigo-reducer'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';

const store = createStore(amigoReducer, applyMiddleware(thunk))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root')
)

// ---------------------------------------------------




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
