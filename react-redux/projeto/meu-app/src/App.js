import React, { Component } from 'react';

import AmigoGerenciar from './containers/GerenciarAmigos'
import Creditos from './components/Creditos'


// REACT ROUTER: incisão....

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div>
        <h1>Meu Aplicativo de Amigos</h1>

        <Router>
          <div>
            <ul>
              <li>
                <Link to="/">Gerenciar Amigos</Link>
              </li>
              <li>
                <Link to="/creditos">Créditos</Link>
              </li>
            </ul>

            <hr/>

            <Route exact path="/" component={AmigoGerenciar} />
            <Route path="/creditos" component={Creditos} />

          </div>
        </Router>
        
      </div>
    );
  }
}

export default App;
