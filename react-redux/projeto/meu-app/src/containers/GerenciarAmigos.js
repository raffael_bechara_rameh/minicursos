import React from 'react'
import { connect } from 'react-redux'

import AmigoLista from '../components/AmigoLista'
import AmigoItemLista from '../components/AmigoItemLista'
import AmigoFormulario from '../containers/AmigoFormulario'

import { adicionarAmigo, excluirAmigo, editarAmigo } from '../actions/Amigo-actions'

const GerenciarAmigos = ({ listaAmigos, onAmigoFormSubmit, onAmigoItemDelete, onAmigoItemEdit, amigoEmEdicao }) => (
    <div>            
        <h2>Gerenciar Amigos</h2>

        <AmigoLista>
            { listaAmigos.map((amigo, i) => ( 
                <AmigoItemLista key={i} amigo={amigo} 
                    onAmigoItemEdit={onAmigoItemEdit}
                    onAmigoItemDelete={onAmigoItemDelete} />
            )) 
            }
        </AmigoLista>

        <AmigoFormulario amigo={amigoEmEdicao} onSubmit={ onAmigoFormSubmit } />
    </div>
)

const mapStateToProps = state => {    
    return {
        listaAmigos: state.listaAmigos,
        amigoEmEdicao: state.amigoEmEdicao
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAmigoFormSubmit: (amigo) => {            
            dispatch(adicionarAmigo(amigo))
        },

        onAmigoItemDelete: (amigo) => {
            dispatch(excluirAmigo(amigo))
        },

        onAmigoItemEdit: (amigo) => {
            dispatch(editarAmigo(amigo))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GerenciarAmigos)