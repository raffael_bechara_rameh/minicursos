
export const adicionarAmigo = ( amigo ) => {
    return {
        type: 'ADICIONAR_AMIGO',
        payload: amigo
    }
}

export const excluirAmigo = ( amigo ) => {
    return {
        type: 'EXCLUIR_AMIGO',
        payload: amigo
    }
}

export const editarAmigo = ( amigo ) => {
    return {
        type: 'EDITAR_AMIGO',
        payload: amigo
    }
}

export const atualizarAmigoEmEdicao = ( amigo ) => {
    return {
        type: 'ATUALIZAR_AMIGO_EM_EDICAO',
        payload: amigo
    }
}

export const salvarAmigoEmEdicao = ( ) => {
    return {
        type: 'SALVAR_AMIGO_EM_EDICAO'
    }
}
