import React from 'react'

const AmigoLista = (props) => (
    <table border="1">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Idade</th>        
            </tr>
        </thead>
        <tbody>
            { props.children }
        </tbody>
    </table>
);

export default AmigoLista;