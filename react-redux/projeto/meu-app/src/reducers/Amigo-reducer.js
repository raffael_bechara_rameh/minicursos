
let amigoCounter = 0

const initialState = {
    listaAmigos: [
        { id: ++amigoCounter, nome: 'Raffael', idade: 36 },
        { id: ++amigoCounter, nome: 'Eduardo', idade: 38 },
    ],

    amigoEmEdicao: {
        editando: false,
        nome: '',
        idade: ''
    }
}

const amigoReducer = (state = initialState, action) => {
    switch (action.type) {

        case 'SALVAR_AMIGO_EM_EDICAO':
            if (state.amigoEmEdicao.editando) {
                const updatedItems = state.listaAmigos.map(item => {
                    if(item.id === state.amigoEmEdicao.id){
                      return { ...item, ...state.amigoEmEdicao }
                    }
                    return item
                  });
                return { 
                    ...state, 
                    listaAmigos: updatedItems, 
                    amigoEmEdicao: initialState.amigoEmEdicao 
                };                

            } else {
                return { 
                    ...state, 
                    listaAmigos: [ ...state.listaAmigos, { ...state.amigoEmEdicao, id: ++amigoCounter} ],
                    amigoEmEdicao: initialState.amigoEmEdicao 
                }    
            }            

        case 'ATUALIZAR_AMIGO_EM_EDICAO':
            return { ...state, amigoEmEdicao: Object.assign({}, state.amigoEmEdicao, action.payload) }

        case 'EXCLUIR_AMIGO':            
            return { ...state, listaAmigos: state.listaAmigos.filter(amigo => amigo !== action.payload) }

        case 'EDITAR_AMIGO':
            return { ...state, amigoEmEdicao: { ...action.payload, editando: true } }
        
        default:
            return state
    }
}

export default amigoReducer;