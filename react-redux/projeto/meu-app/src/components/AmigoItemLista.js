import React from 'react'

const AmigoItemLista = ({ amigo, onAmigoItemEdit, onAmigoItemDelete }) => (
    <tr>
        <td>{ amigo.id }</td>
        <td>{ amigo.nome }</td>
        <td>{ amigo.idade }</td>
        
        <td>
            <input type="button" onClick={e => onAmigoItemEdit(amigo)} value="Editar" />
            <input type="button" onClick={e => onAmigoItemDelete(amigo)} value="Excluir" />
        </td>
    </tr>
);

export default AmigoItemLista;