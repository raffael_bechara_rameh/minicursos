import React from 'react'

import { connect } from 'react-redux'
import { salvarAmigoEmEdicao, editarAmigo, atualizarAmigoEmEdicao } from '../actions/Amigo-actions'

const AmigoFormulario = ({ amigoEmEdicao, onAmigoFormSubmit, handleChange }) => (
    <form onSubmit={ onAmigoFormSubmit }>
        <p>
            <label htmlFor="nome">Nome:</label> <br />
            <input required type="text" id="nome" name="nome" value={amigoEmEdicao.nome} onChange={handleChange} />
        </p>

        <p>
            <label htmlFor="idade">Idade:</label> <br />
            <input required type="number" id="idade" name="idade" value={amigoEmEdicao.idade} onChange={handleChange}/>
        </p>

        <p>
            <input type="submit" value={amigoEmEdicao.editando ? 'Salvar' : 'Adicionar'} />
        </p>                
    </form>
)

const mapStateToProps = state => {    
    return {
         amigoEmEdicao: state.amigoEmEdicao
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAmigoFormSubmit: ( evt ) => {
            evt.preventDefault()
            dispatch(salvarAmigoEmEdicao())
        },

        handleChange: ( evt ) => {
            let amigo = { [evt.target.name]: evt.target.value }
            dispatch(atualizarAmigoEmEdicao(amigo))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AmigoFormulario)